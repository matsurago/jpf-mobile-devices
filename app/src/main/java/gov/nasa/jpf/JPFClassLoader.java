/*
 * Copyright (C) 2014, United States Government, as represented by the
 * Administrator of the National Aeronautics and Space Administration.
 * All rights reserved.
 *
 * The Java Pathfinder core (jpf-core) platform is licensed under the
 * Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 *        http://www.apache.org/licenses/LICENSE-2.0. 
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package gov.nasa.jpf;

import dalvik.system.BaseDexClassLoader;
import dalvik.system.PathClassLoader;

import java.io.File;
import java.lang.reflect.Field;
import java.net.URL;

/**
 * classloader that is used by Config to instantiate from JPF configured
 * paths. This is a standard parent-first loader to avoid multiple class
 * instances when using our Run*.jar tools
 *
 * The main reason for having our own classloader is dynamically configured resource
 * and library lookup
 */
//!alex: URLClassLoader -> PathClassLoader
public class JPFClassLoader extends PathClassLoader { //URLClassLoader {
//!:alex

  String[] nativeLibs;


  static {
    //ClassLoader.registerAsParallelCapable(); // for jdk7
  }

  public JPFClassLoader (URL[] urls){
    //!alex: seems there are no usages
    //super(urls);
    super(null, null);
    throw new RuntimeException("Stub!");
    //!:alex
  }

  public JPFClassLoader (URL[] urls, String[] libs, ClassLoader parent){
    //!alex:
    super(urlsToString(urls), stringArrayToString(libs), parent);
    //super(urls, parent);
    //!:alex

    nativeLibs = libs;
  }

  //!alex: utility functions
  private static String urlsToString(URL[] urls) {
    String result = "";

    if (urls != null && urls.length != 0) {
      result = urls[0].getPath();
      for (int i = 1; i < urls.length; i++)
        result += ":" + urls[i].getPath();
    }

    return result;
  }

  private static String stringArrayToString(String[] array) {
    String result = "";

    if (array != null && array.length != 0) {
      result = array[0];
      for (int i = 1; i < array.length; i++)
        result += ":" + array[i];
    }

    return result;
  }
  //!:alex

  @Override
  //!alex: protected -> public
  public String findLibrary (String libBaseName){  //protected
  //!:alex

    if (nativeLibs != null){
      String libName = File.separator + System.mapLibraryName(libBaseName);

      for (String libPath : nativeLibs) {
        if (libPath.endsWith(libName)) {
          return libPath;
        }
      }
    }

    return null; // means VM uses java.library.path to look it up
  }

  /**
   * we make it public since we add paths dynamically during JPF init
   * 
   * Note this is ignored according to the javadocs if the provided url is already in the classpath.
   * We do rely on this feature since me might add jpf.jar several times during bootstrap
   */
  //!alex: removed @Override
  public void addURL (URL url){
  //!:alex
    if (url != null){
      //!alex: we can't add URLs to PathClassLoader at the moment ...
      //super.addURL(url);
      throw new RuntimeException("Stub!");
      //!:alex
    }
  }
  
  public void setNativeLibs (String[] libs){
    nativeLibs = libs;
  }
}
