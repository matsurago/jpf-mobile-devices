package jp.ac.chiba_u.s.math.jpf;

import android.content.Context;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.File;

/**
 * The class encompasses functions to translate console JPF output
 * to Android app's window.
 */
public final class Relay {
  private static Context sContext = null;
  private static String externalFilesDirPath = System.getProperty("user.dir");

  private static Relay mInstance = null;
  public final static RelayPrintStream out = new RelayPrintStream(System.out);
  public final static RelayPrintStream err = new RelayPrintStream(System.out);


  public static void init(TextView dest, ScrollView scrollView, Context ctx) {
    sContext = ctx;

    File externalFilesDir = sContext.getExternalFilesDir(null);
    if (externalFilesDir != null) {
      externalFilesDirPath = externalFilesDir.getPath();
    }

    out.setOutputWindow(dest, scrollView);
  }

  public static String getProperty(String property) {
    switch (property) {
      case "user.dir":
      case "user.home":
        return getHomeDir();
      default:
        return System.getProperty(property);
    }
  }


  /**
   * Gets the user-accessible home directory of jpf-mobile,
   * where JPF property files and examples are kept.
   *
   * @return home dir
   */
  public static String getHomeDir() {
    return externalFilesDirPath;
  }


  public static Context getContext() {
    return sContext;
  }
}
