package jp.ac.chiba_u.s.math.jpf.jvm;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;

import dalvik.system.DexFile;
import gov.nasa.jpf.jvm.JVMClassFileContainer;
import gov.nasa.jpf.vm.AnnotationInfo;
import gov.nasa.jpf.vm.ClassFileContainer;
import gov.nasa.jpf.vm.ClassFileMatch;
import gov.nasa.jpf.vm.ClassInfo;
import gov.nasa.jpf.vm.ClassLoaderInfo;
import gov.nasa.jpf.vm.ClassParseException;

/**
 * DexClassFileContainer
 */
public class DexClassFileContainer extends JVMClassFileContainer {
  protected DexFile mDexFile;
  protected Collection<String> mDexEntries;

  public class DexClassFileMatch extends ClassFileMatch {

    protected DexClassFileMatch(String typeName, String url) {
      super(typeName, url);
    }

    @Override
    public ClassFileContainer getContainer() {
      return null;
    }

    @Override
    public ClassInfo createClassInfo(ClassLoaderInfo loader) throws ClassParseException {
      return null;
    }

    @Override
    public AnnotationInfo createAnnotationInfo(ClassLoaderInfo loader) throws ClassParseException {
      return null;
    }
  }


  protected DexClassFileContainer(File file) throws IOException {
    super(file.getPath(), file.getAbsolutePath());

    mDexFile = new DexFile(file);
    mDexEntries = Collections.list(mDexFile.entries());
  }

  @Override
  public ClassFileMatch getMatch(String clsName) throws ClassParseException {
    String pn = clsName.replace('.', '/');

    if (mDexEntries.contains(pn)) {


    }

    return null;
  }

}
