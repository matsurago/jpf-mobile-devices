package jp.ac.chiba_u.s.math.jpf;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

/**
 * An Activity for convenient JPF extension selection.
 */
public class ExtensionsActivity extends AppCompatActivity {

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_extensions); // inflate UI

    RecyclerView extensionListView = (RecyclerView) findViewById(R.id.jpf_extension_list);
    // ListView requires a layout manager to work
    extensionListView.setLayoutManager(new LinearLayoutManager(this));

    // get list of JPF extensions from settings
    List<ExtensionDescriptor> extensionList = Settings.getInstance().getJpfExtensionList();
    // pass JPF extension list to an adapter
    extensionListView.setAdapter(new ExtensionListAdapter(extensionList));
  }

  private class ExtensionListAdapter extends RecyclerView.Adapter<ExtensionItemHolder> {
    private List<ExtensionDescriptor> extensionList;

    private ExtensionListAdapter(List<ExtensionDescriptor> extensionList) {
      this.extensionList = extensionList;
    }

    @Override
    public ExtensionItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = getLayoutInflater().inflate(R.layout.list_item_extension_holder, parent, false);
      return new ExtensionItemHolder(view);
    }

    @Override
    public void onBindViewHolder(ExtensionItemHolder itemHolder, int position) {
      ExtensionDescriptor descriptor = extensionList.get(position);
      itemHolder.bind(descriptor);
    }

    @Override
    public int getItemCount() {
      return extensionList.size();
    }
  }

  private class ExtensionItemHolder extends RecyclerView.ViewHolder {
    private ExtensionDescriptor descriptor;

    private TextView extensionNameField;
    private CheckBox isExtensionEnabledCheckBox;

    public ExtensionItemHolder(View itemView) {
      super(itemView);

      // init UI elements
      extensionNameField = (TextView) itemView.findViewById(R.id.extension_list_name_label);
      isExtensionEnabledCheckBox = (CheckBox) itemView.findViewById(R.id.extension_list_enabled_checkbox);

      // user actions on UI
      isExtensionEnabledCheckBox.setOnClickListener(view -> {
        descriptor.setEnabled(isExtensionEnabledCheckBox.isChecked());
        Settings.getInstance().notifyExtensionListChanged();
      });
    }

    // model -> view
    public void bind(ExtensionDescriptor d) {
      descriptor = d;

      extensionNameField.setText(d.getName());
      isExtensionEnabledCheckBox.setChecked(d.isEnabled());

      if (descriptor.getName().equals(Settings.S_EXT_CORE_NAME))
        isExtensionEnabledCheckBox.setEnabled(false);
    }
  }

}
