package jp.ac.chiba_u.s.math.jpf;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import gov.nasa.jpf.tool.RunJPF;

/**
 * Executes jpf-core in the background.
 */
public class JpfCoreService extends IntentService {
  private static final String TAG = "JpfCoreService";
  private static final String EXTRA_JPF_ARGS = "jp.ac.chiba_u.s.math.jpf.JpfCoreService_jpfArgs";
  private boolean mRunning = false;

  public JpfCoreService() {
    super(TAG);
  }

  // Returns an Intent to start this service.
  public static Intent newIntent(Context context, String[] jpfArgs) {
    Intent i = new Intent(context, JpfCoreService.class);
    i.putExtra(EXTRA_JPF_ARGS, jpfArgs);
    return i;
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    String[] args = intent.getStringArrayExtra(EXTRA_JPF_ARGS);

    if (args == null) {
      Relay.out.println("[JpfCoreService] No jpf-core args were set for the calling Intent.");
      return;
    }

    try {
      RunJPF.main(args);
    } catch (Throwable e) {
      Relay.out.println(e.getMessage());
    }
  }

  @Override
  public void onDestroy() {
    MessagePollThread.enable(false);
  }
}
