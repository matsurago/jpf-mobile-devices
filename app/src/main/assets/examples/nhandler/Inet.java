import java.net.InetAddress;

public class Inet {
  public static void main(String[] args) throws Exception {
    //String addr = InetAddress.getLocalHost().getHostName();
    String addr = InetAddress.getLoopbackAddress().getHostName();
    System.out.println("Local Address: " + addr);
  }
}
